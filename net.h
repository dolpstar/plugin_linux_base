/*** xrkmonitor license ***

   Copyright (c) 2019 by rockdeng

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   字符云监控(xrkmonitor) 开源版 (c) 2019 by rockdeng
   当前版本：v1.0
   使用授权协议： apache license 2.0

   云版本主页：http://xrkmonitor.com

   云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
   微信等多种方式，欢迎使用

   内置监控插件 linux_base 功能:
   		使用监控系统 api 实现 linux 基础信息监控上报, 包括 cpu/内存/磁盘/网络

****/

#ifndef __MTREPORT_NET_H__
#define __MTREPORT_NET_H__ 1

typedef struct 
{
	char szInterName[32];
	uint64_t qwRecvPackets;
	uint64_t qwSendPackets;
	uint64_t qwRecvBytes;
	uint64_t qwSendBytes;
	uint32_t dwRecvDropPackets;
	uint32_t dwSendDropPackets;
}TNetIfInfo;

typedef struct
{
    bool bNotSupport;
    uint32_t dwUdpPackIn;
    uint32_t dwUdpPackOut;

    uint32_t dwUdpInErr;
    uint32_t dwUdpNoPorts;
    uint32_t dwUdpRecvBufErr;
    uint32_t dwUdpSendBufErr;

    uint32_t dwTcpPackIn;
    uint32_t dwTcpPackOut;
    int32_t iCurTcpEstab;

    uint32_t dwTcpReSends;
    uint32_t dwTcpInErr;
    uint32_t dwTcpOutRsts;
}TTcpUdpInfo;

typedef struct
{
    uint32_t dwUdpPackIn;
    uint32_t dwUdpPackOut;
    uint32_t dwTcpPackIn;
    uint32_t dwTcpPackOut;
    int32_t iCurTcpEstab;
    int32_t iUdpErr;
    int32_t iTcpErr;
}TNetUseInfo;

void InitGetNet();
int GetNetTcpUdpInfo(TTcpUdpInfo *pstNetTcpUdpInfo);
void ReportNetInfo();

typedef struct {
    uint32_t dwDiskTotalSpace;
    int32_t iDiskUsePercent;
    uint32_t dwMemSpace;
    int32_t iMemUsePercent;
    int32_t iCpuUsePercent;
    uint64_t qwSendBytes;
    uint64_t qwRecvBytes;
}TBaseInfo;


#endif

