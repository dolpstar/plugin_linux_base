/*********************************************************************

	该文件为字符云监控系统插件源码头文件, 跟插件版本相关
	请勿在该文件中添加您的私有代码, 以免版本升级时影响代码合并

	插件显示名: linux 基础资源监控
	插件部署名: linux_base
	插件ID: 12
	文件版本: v1.0.37
	使用授权协议  Apache-2.0

 ***********************************************************************/

#ifndef _XRKMONITOR_PLUGIN_12_H_
#define _XRKMONITOR_PLUGIN_12_H_ 1 

#include <sv_cfg.h>
#include <mt_log.h>
#include <sv_coredump.h>

#define XRK_PLUGIN_HEADER_FILE_VER "v1.0.37" 
#define XRK_PLUGIN_ID 12 
#define XRK_PLUGIN_NAME "linux_base"
#define CORE_INFO_FILE "./core_linux_base.log" 
extern MtReport g_mtReport;
extern int g_aryPluginAttr[80]; 
#define NETIF_DROP_PACK g_aryPluginAttr[0]
#define LO_OUT_BYTES g_aryPluginAttr[1]
#define LO_IN_BYTES g_aryPluginAttr[2]
#define LO_OUT_PACK g_aryPluginAttr[3]
#define LO_IN_PACK g_aryPluginAttr[4]
#define ETH1_OUT_BYTES g_aryPluginAttr[5]
#define ETH1_IN_BYTES g_aryPluginAttr[6]
#define ETH1_OUT_PACK g_aryPluginAttr[7]
#define ETH1_IN_PACK g_aryPluginAttr[8]
#define ETH0_OUT_BYTES g_aryPluginAttr[9]
#define ETH0_IN_BYTES g_aryPluginAttr[10]
#define ETH0_OUT_PACK g_aryPluginAttr[11]
#define ETH0_IN_PACK g_aryPluginAttr[12]
#define ETH_COMM1_OUT_BYTES g_aryPluginAttr[13]
#define ETH_COMM1_IN_BYTES g_aryPluginAttr[14]
#define ETH_COMM1_OUT_PACK g_aryPluginAttr[15]
#define ETH_COMM1_IN_PACK g_aryPluginAttr[16]
#define ETH_COMM0_OUT_BYTES g_aryPluginAttr[17]
#define ETH_COMM0_IN_BYTES g_aryPluginAttr[18]
#define ETH_COMM0_OUT_PACK g_aryPluginAttr[19]
#define ETH_COMM0_IN_PACK g_aryPluginAttr[20]
#define ETHTOTAL_OUT_BYTES g_aryPluginAttr[21]
#define ETHTOTAL_IN_BYTES g_aryPluginAttr[22]
#define ETHTOTAL_OUT_PACK g_aryPluginAttr[23]
#define ETHTOTAL_IN_PACK g_aryPluginAttr[24]
#define XRK_ATTR_CPU15_USE g_aryPluginAttr[25]
#define XRK_ATTR_CPU14_USE g_aryPluginAttr[26]
#define XRK_ATTR_CPU13_USE g_aryPluginAttr[27]
#define XRK_ATTR_CPU12_USE g_aryPluginAttr[28]
#define XRK_ATTR_CPU11_USE g_aryPluginAttr[29]
#define XRK_ATTR_CPU10_USE g_aryPluginAttr[30]
#define XRK_ATTR_CPU9_USE g_aryPluginAttr[31]
#define XRK_ATTR_CPU8_USE g_aryPluginAttr[32]
#define XRK_ATTR_CPU7_USE g_aryPluginAttr[33]
#define XRK_ATTR_CPU6_USE g_aryPluginAttr[34]
#define XRK_ATTR_CPU5_USE g_aryPluginAttr[35]
#define XRK_ATTR_CPU4_USE g_aryPluginAttr[36]
#define XRK_ATTR_CPU3_USE g_aryPluginAttr[37]
#define XRK_ATTR_CPU2_USE g_aryPluginAttr[38]
#define XRK_ATTR_CPU1_USE g_aryPluginAttr[39]
#define XRK_ATTR_CPU0_USE g_aryPluginAttr[40]
#define XRK_ATTR_CPU_TOAL_USE g_aryPluginAttr[41]
#define XRK_MEM_USE g_aryPluginAttr[42]
#define XRK_DISK_USE_MAX g_aryPluginAttr[43]
#define XRK_DISK_USE_OVER_95 g_aryPluginAttr[44]
#define XRK_DISK_USE_OVER_90 g_aryPluginAttr[45]
#define XRK_DISK_USE_OVER_80 g_aryPluginAttr[46]
#define XRK_DISK_USE_OVER_70 g_aryPluginAttr[47]
#define XRK_DISK_TOTAL_USE g_aryPluginAttr[48]
#define XRK_CPU_TOTAL_USER g_aryPluginAttr[49]
#define XRK_CPU_TOTAL_SOFTIRQ g_aryPluginAttr[50]
#define XRK_CPU_TOTAL_IRQ g_aryPluginAttr[51]
#define XRK_CPU_TOTAL_SYS g_aryPluginAttr[52]
#define XRK_CPU_TOTAL_IOWA g_aryPluginAttr[53]
#define XRK_CPU_TOTAL_IDLE g_aryPluginAttr[54]
#define XRK_CPU_LOAD_AVG g_aryPluginAttr[55]
#define XRK_TOTAL_CPU_CTXT g_aryPluginAttr[56]
#define XRK_PROC_CREATE_COUNT g_aryPluginAttr[57]
#define XRK_UDP_OUT_PACKS g_aryPluginAttr[58]
#define XRK_UDP_IN_PACKS g_aryPluginAttr[59]
#define XRK_TCP_OUT_PACKS g_aryPluginAttr[60]
#define XRK_TCP_IN_PACKS g_aryPluginAttr[61]
#define XRK_TCP_CONN_COUNT g_aryPluginAttr[62]
#define XRK_DISK_IO_USE_MS g_aryPluginAttr[63]
#define XRK_DISK_MERGE_W_TIMES g_aryPluginAttr[64]
#define XRK_DISK_MERGE_R_TIMES g_aryPluginAttr[65]
#define XRK_DISK_W_SECTORS g_aryPluginAttr[66]
#define XRK_DISK_R_SECTORS g_aryPluginAttr[67]
#define XRK_DISK_W_TIMES g_aryPluginAttr[68]
#define XRK_DISK_R_TIMES g_aryPluginAttr[69]
#define XRK_MEM_AVAILABLE g_aryPluginAttr[70]
#define XRK_SWAP_MEM_USE g_aryPluginAttr[71]
#define XRK_DISK_W_USE_MS g_aryPluginAttr[72]
#define XRK_DISK_R_USE_MS g_aryPluginAttr[73]
#define XRK_UDP_PACKS_ERROR g_aryPluginAttr[74]
#define XRK_TCP_PACKS_ERROR g_aryPluginAttr[75]
#define XRK_CPU_TOTAL_GUESTNI g_aryPluginAttr[76]
#define XRK_CPU_TOTAL_GUEST g_aryPluginAttr[77]
#define XRK_CPU_TOTAL_STEAL g_aryPluginAttr[78]
#define XRK_CPU_TOTAL_NICE g_aryPluginAttr[79]
#define XRK_PLUGIN_ATTRS_COUNT_MAX 80 
#define XRK_PLUGIN_ALL_ATTRS "NETIF_DROP_PACK LO_OUT_BYTES LO_IN_BYTES LO_OUT_PACK LO_IN_PACK ETH1_OUT_BYTES ETH1_IN_BYTES ETH1_OUT_PACK ETH1_IN_PACK ETH0_OUT_BYTES ETH0_IN_BYTES ETH0_OUT_PACK ETH0_IN_PACK ETH_COMM1_OUT_BYTES ETH_COMM1_IN_BYTES ETH_COMM1_OUT_PACK ETH_COMM1_IN_PACK ETH_COMM0_OUT_BYTES ETH_COMM0_IN_BYTES ETH_COMM0_OUT_PACK ETH_COMM0_IN_PACK ETHTOTAL_OUT_BYTES ETHTOTAL_IN_BYTES ETHTOTAL_OUT_PACK ETHTOTAL_IN_PACK XRK_ATTR_CPU15_USE XRK_ATTR_CPU14_USE XRK_ATTR_CPU13_USE XRK_ATTR_CPU12_USE XRK_ATTR_CPU11_USE XRK_ATTR_CPU10_USE XRK_ATTR_CPU9_USE XRK_ATTR_CPU8_USE XRK_ATTR_CPU7_USE XRK_ATTR_CPU6_USE XRK_ATTR_CPU5_USE XRK_ATTR_CPU4_USE XRK_ATTR_CPU3_USE XRK_ATTR_CPU2_USE XRK_ATTR_CPU1_USE XRK_ATTR_CPU0_USE XRK_ATTR_CPU_TOAL_USE XRK_MEM_USE XRK_DISK_USE_MAX XRK_DISK_USE_OVER_95 XRK_DISK_USE_OVER_90 XRK_DISK_USE_OVER_80 XRK_DISK_USE_OVER_70 XRK_DISK_TOTAL_USE XRK_CPU_TOTAL_USER XRK_CPU_TOTAL_SOFTIRQ XRK_CPU_TOTAL_IRQ XRK_CPU_TOTAL_SYS XRK_CPU_TOTAL_IOWA XRK_CPU_TOTAL_IDLE XRK_CPU_LOAD_AVG XRK_TOTAL_CPU_CTXT XRK_PROC_CREATE_COUNT XRK_UDP_OUT_PACKS XRK_UDP_IN_PACKS XRK_TCP_OUT_PACKS XRK_TCP_IN_PACKS XRK_TCP_CONN_COUNT XRK_DISK_IO_USE_MS XRK_DISK_MERGE_W_TIMES XRK_DISK_MERGE_R_TIMES XRK_DISK_W_SECTORS XRK_DISK_R_SECTORS XRK_DISK_W_TIMES XRK_DISK_R_TIMES XRK_MEM_AVAILABLE XRK_SWAP_MEM_USE XRK_DISK_W_USE_MS XRK_DISK_R_USE_MS XRK_UDP_PACKS_ERROR XRK_TCP_PACKS_ERROR XRK_CPU_TOTAL_GUESTNI XRK_CPU_TOTAL_GUEST XRK_CPU_TOTAL_STEAL XRK_CPU_TOTAL_NICE "

#define XRK_TABLE_BASE_INFO 1
#define XRK_TABLE_BASE_INFO_DEF_STAT_TIME 60
#define XRK_FLD_DISK_TOTAL_SPACE 6
#define XRK_FLD_DISK_USE 3
#define XRK_FLD_MEM_SIZE 7
#define XRK_FLD_MEM_USE 2
#define XRK_FLD_CPU_INFO 8
#define XRK_FLD_CPU_USE 1
#define XRK_FLD_NET_T_SENDB 9
#define XRK_FLD_NET_T_RECVB 10

#define XRK_TABLE_NET_TU_PACK_STAT 7
#define XRK_TABLE_NET_TU_PACK_STAT_DEF_STAT_TIME 60
#define XRK_FLD_TCP_IN_COUNT 1
#define XRK_FLD_TCP_OUT_COUNT 2
#define XRK_FLD_TCP_ERR_COUNT 3
#define XRK_FLD_TCP_CONN_COUNT 4
#define XRK_FLD_UDP_IN_COUNT 5
#define XRK_FLD_UDP_OUT_COUNT 6
#define XRK_FLD_UDP_ERR_COUNT 7

#define XRK_TABLE_DISK_STAT_ALL 3
#define XRK_TABLE_DISK_STAT_ALL_DEF_STAT_TIME 60
#define XRK_FLD_DISK_PART_NAME 1
#define XRK_FLD_DISK_MOUNT 12
#define XRK_FLD_DISK_SPACE 13
#define XRK_FLD_DISK_R_TIMES 2
#define XRK_FLD_DISK_W_TIMES 6
#define XRK_FLD_DISK_R_SECTORS 4
#define XRK_FLD_DISK_W_SECTORS 8
#define XRK_FLD_DISK_R_USE_MS 5
#define XRK_FLD_DISK_W_USE_MS 9
#define XRK_FLD_DISK_IO_USE_MS 10
#define XRK_FLD_DISK_USE_PERCENT 11

#define XRK_TABLE_NET_DEV_ALL 2
#define XRK_TABLE_NET_DEV_ALL_DEF_STAT_TIME 60
#define XRK_FLD_NET_DEV_NAME 1
#define XRK_FLD_NET_DEV_RECV_BYTES 2
#define XRK_FLD_NET_DEV_SEND_BYTES 3
#define XRK_FLD_NET_DEV_RECV_PACKS 4
#define XRK_FLD_NET_DEV_SEND_PACKS 5

#define XRK_TABLE_CPU_ALL_STAT 8
#define XRK_TABLE_CPU_ALL_STAT_DEF_STAT_TIME 60
#define XRK_FLD_CPU_NUM 1
#define XRK_FLD_CPU_USER 2
#define XRK_FLD_CPU_NICE 3
#define XRK_FLD_CPU_SYS 4
#define XRK_FLD_CPU_IOWA 5
#define XRK_FLD_CPU_IRQ 6
#define XRK_FLD_CPU_SOFTIRQ 7
#define XRK_FLD_CPU_GUEST 8
#define XRK_FLD_CPU_GUESTNICE 9
#define XRK_FLD_CPU_STAT_USE 10

#define XRK_TABLE_MEM_STAT 9
#define XRK_TABLE_MEM_STAT_DEF_STAT_TIME 60
#define XRK_FLD_MEM_TOTAL 1
#define XRK_FLD_MEM_FREE 2
#define XRK_FLD_MEM_BUFF 3
#define XRK_FLD_MEM_CACHE 4
#define XRK_FLD_MEM_AVAILABLE 5
#define XRK_FLD_MEM_SWAP_TOTAL 6
#define XRK_FLD_MEM_SWAP_FREE 7
#define XRK_FLD_MEM_SHM 8
#define XRK_FLD_MEM_T_USE_PERCENT 9


#define MAX_CPU_SUPPORT 16

#endif

