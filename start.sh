#!/bin/bash

rm _manual_stop_ > /dev/null 2>&1
proc=xrk_linux_base

Count=`pgrep -f xrk_linux_base -l |wc -l`
if [ $Count -gt 0 ]; then
    echo "already start"
    exit 0
fi

./$proc

pid=`ps -ef |grep $proc|grep -v tail|grep -v grep|awk '{print $2;}'`
if [ -z "$pid" ]; then
    echo "start failed !"
	exit 1
fi

echo "start ok pid: "
echo "$pid"

