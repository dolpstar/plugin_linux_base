/*** xrkmonitor license ***

   Copyright (c) 2019 by rockdeng

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   字符云监控(xrkmonitor) 开源版 (c) 2019 by rockdeng
   当前版本：v1.0
   使用授权协议： apache license 2.0

   云版本主页：http://xrkmonitor.com

   云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
   微信等多种方式，欢迎使用

   内置监控插件 linux_base 功能:
   		使用监控系统 api 实现 linux 基础信息监控上报, 包括 cpu/内存/磁盘/网络

****/

#ifndef __MTREPORT_CPU_H__
#define __MTREPORT_CPU_H__ 1

#include "xrk_linux_base.h"

typedef struct {
    char szCpuName[32];
	uint64_t qwUser;
	uint64_t qwNice;
	uint64_t qwSys;
	uint64_t qwIdle;
	uint64_t qwIowait;
	uint64_t qwIrq;
	uint64_t qwSoftIrq;
    uint64_t qwSteal;
    uint64_t qwGuest;
    uint64_t qwGuestNice;
	uint64_t qwTotal;
}TCpuInfo;

#define CPU_AWK_FMT "$1\" \"$2\" \"$3\" \"$4\" \"$5\" \"$6\" \"$7\" \"$8\" \"$9\" \"$10\" \"$11"
#define CPU_USE(c1, c2) (1000-((c2).qwIdle-(c1).qwIdle)*1000/((c2).qwTotal-(c1).qwTotal))
#define CPU_USER(c1, c2) (((c2).qwUser-(c1).qwUser)*1000/((c2).qwTotal-(c1).qwTotal))
#define CPU_NICE(c1, c2) (((c2).qwNice-(c1).qwNice)*1000/((c2).qwTotal-(c1).qwTotal))
#define CPU_SYS(c1, c2) (((c2).qwSys-(c1).qwSys)*1000/((c2).qwTotal-(c1).qwTotal))
//#define CPU_IDLE(c1, c2) (((c2).qwIdle-(c1).qwIdle)*1000/((c2).qwTotal-(c1).qwTotal))
#define CPU_IO(c1, c2) (((c2).qwIowait-(c1).qwIowait)*1000/((c2).qwTotal-(c1).qwTotal))
#define CPU_IRQ(c1, c2) (((c2).qwIrq-(c1).qwIrq)*1000/((c2).qwTotal-(c1).qwTotal))
#define CPU_SOFTIRQ(c1, c2) (((c2).qwSoftIrq-(c1).qwSoftIrq)*1000/((c2).qwTotal-(c1).qwTotal))
#define CPU_STEAL(c1, c2) (((c2).qwSteal-(c1).qwSteal)*1000/((c2).qwTotal-(c1).qwTotal))
#define CPU_GUEST(c1, c2) (((c2).qwGuest-(c1).qwGuest)*1000/((c2).qwTotal-(c1).qwTotal))
#define CPU_GUESTNI(c1, c2) (((c2).qwGuestNice-(c1).qwGuestNice)*1000/((c2).qwTotal-(c1).qwTotal))

typedef struct {
	int32_t iCpuCount;
    uint64_t qwCtxt;
    uint32_t dwProcessCount;
	TCpuInfo sInfo[1+MAX_CPU_SUPPORT];
}TCpuStatic;

typedef struct {
	int32_t iCpuCount;
	int32_t iCpuUse[1+MAX_CPU_SUPPORT]; // 使用率在 0-1000 之间，千分制
    int32_t iCpuUser;
    int32_t iCpuNice;
    int32_t iCpuSys;
    int32_t iCpuIdle;
    int32_t iCpuIoWa;
    int32_t iCpuIrq;
    int32_t iCpuSoftIrq;
    int32_t iCpuSteal;
    int32_t iCpuGuest;
    int32_t iCpuGuestNice;
    uint8_t bStaticTimes; // 统计次数，用于计算使用率均值
    uint32_t dwProcessNew;
    uint32_t dwCtxt;
}TcpuUse;

int InitGetCpuUse();

// 从 /proc/stat 中获取cpu 采样数据
int GetCpuStatis(TCpuStatic *pcp);

// 通过两次采样数据，计算cpu 使用率
int GetCpuUse(TcpuUse *pCpuUse);

// 平均负载
void WriteCpuLoadAvg();
void GetCpuInfo();
#endif



