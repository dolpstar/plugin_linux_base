#
# 手动编译部署插件说明（推荐您使用一键部署功能，一键帮您部署插件到机器）
#
# 插件编译说明：
# 云版本: 请在首页“下载” 库文件，拷贝到编译机解压后执行 install.sh 初始化编译环境
# 开源版：在码云下载开源版源码，进入源码目录 language/c_cpp  执行 make; make install 初始化编译环境
#
# 编译环境初始化后，下载插件源码解压并执行 make 即可生成插件可执行文件
# 执行 ./make_fabu.sh open 生成开源版部署包
# 执行 ./make_fabu.sh cloud 生成云版部署包
# 将部署包拷贝到机器上解压执行 ./start.sh 即可启动插件, 执行 ./add_crontab.sh 添加crontab 执行监控
#
# 静态库文件为：/usr/lib64/libmtreport_api.a 或者 ${MTLIB_LIB_PATH}/libmtreport_api.a
# 头文件为：/usr/include/mtreport_api(开源版), /usr/include/mtagent_api_lib(云版)
#
# 编译环境只能是云版本环境或者开源版环境，不能同时存在, make 会通过头文件路径检查环境是否正常
# 
#
all_check=$(shell if [ -d /usr/include/mtreport_api -a -d /usr/include/mtagent_api_lib ]; then echo "1"; else echo "2"; fi;)
all_check_no=$(shell if [ ! -d /usr/include/mtreport_api -a ! -d /usr/include/mtagent_api_lib ]; then echo "1"; else echo "2"; fi;)
ifeq ($(all_check), 1)
$(error unknow to make plugin for open or cloud monitor version !)
else ifeq ($(all_check_no), 1)
$(error please init xrkmonitor environment !)
else
WARNINGS := -Wall -Wsign-compare -Wno-strict-aliasing -Wno-write-strings -Wno-array-bounds -Wno-unused-but-set-variable -fpermissive
COMM_INC = -I/usr/include/mtreport_api -I/usr/include/mtagent_api_lib
COMM_LIB = /usr/lib64/libmtreport_api.a 
endif

ifeq ($(findstring debug, $(makecmd)), debug)
	WARNINGS += -g -D_XRK_DEBUG_PLUGIN_
endif

CXXFLAGS_LIB = $(WARNINGS)  
CXXFLAGS_LINK = 
TARGETS = xrk_linux_base 

PLUGIN_NAME = linux_base
INSTALL_PACK = linux_base.tar.gz

CC = g++ 
CXXFLAGS_LIB += $(COMM_INC)

all:$(TARGETS)

SRC := $(wildcard *.cpp)
OBJ := $(SRC:.cpp=.o)

.cpp.o:
	$(CC) $(CXXFLAGS_LIB) -c $(filter %.cpp, $^)

$(TARGETS): ${OBJ} 
	$(CC) $(CXXFLAGS_LIB) $(CXXFLAGS_LINK) -o $@ $^  $(COMM_LIB)
ifneq ($(findstring debug, $(makecmd)), debug)
	strip $(TARGETS)
endif

clean:
	rm -f *.o
	rm -f $(TARGETS) 

pack:
	[ ! -d ${PLUGIN_NAME} ] && mkdir ${PLUGIN_NAME}
	cp *.conf ${PLUGIN_NAME} 
	cp $(TARGETS) ${PLUGIN_NAME} 
	tar -czf ${INSTALL_PACK} ${PLUGIN_NAME}

	
