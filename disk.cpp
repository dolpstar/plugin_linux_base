/*** xrkmonitor license ***

   Copyright (c) 2019 by rockdeng

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   字符云监控(xrkmonitor) 开源版 (c) 2019 by rockdeng
   当前版本：v1.0
   使用授权协议： apache license 2.0

   云版本主页：http://xrkmonitor.com

   云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
   微信等多种方式，欢迎使用

   内置监控插件 linux_base 功能:
   		使用监控系统 api 实现 linux 基础信息监控上报, 包括 cpu/内存/磁盘/网络

****/

#define __STDC_FORMAT_MACROS
#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/shm.h>
#include <math.h>
#include <mt_report.h>
#include "disk.h"
#include <map>
#include <iostream>

extern std::map<std::string, TDiskInfo> g_stDiskInfo;
int GetDiskInfo(uint64_t & qwTotalSpace, uint64_t & qwTotalUse, uint32_t &maxUsePer)
{
    static bool bNotSupport = false;
    if(bNotSupport)
        return -1;

    char sTmpName[256] = {0};
    char sTmpBuf[512] = {0};
	FILE *fp = popen("df -P -l -x devtmpfs -x tmpfs "
        " -x iso9660 -x iso9600 | awk \'{if(NR!=1 && NF==6) print $1\" \"$2\" \"$3\" \"$4\" \"$6}\'", "r");
	if(fp == NULL) {
        MtReport_Log_Error("popen failed, msg:%s", strerror(errno));
        bNotSupport = true;
		return -1;
	}

	qwTotalSpace = 0;
	qwTotalUse = 0;
	maxUsePer = 0;
	uint64_t qwRemain=0, qwUse=0, qwSizeK=0;
	uint32_t dwUsePer = 0;
    TDiskInfo stDisk;
    size_t pos = 0;
    g_stDiskInfo.clear();

    // $2 表示整个分区大小，$3 表示已使用空间，$4 表示可用空间，一般 $2 > $3+$4(分区的某些空间不能使用)
	while( fscanf(fp, "%s %"PRIu64" %"PRIu64" %"PRIu64" %s", sTmpName, &qwSizeK, &qwUse, &qwRemain, sTmpBuf) == 5) {
		qwTotalSpace += qwRemain+qwUse;
		qwTotalUse += qwUse;
		dwUsePer = ceil(qwUse*100.0/(qwRemain+qwUse));
		if(dwUsePer > maxUsePer)
			maxUsePer = dwUsePer;

        // for realtime table
        std::string str(sTmpName), strDev(sTmpName);
        if((pos=str.rfind('/')) != std::string::npos) 
            strDev = str.substr(pos+1);
        stDisk.qwSizeK = qwSizeK;
        stDisk.qwUsed = qwUse;
        stDisk.qwRemain = qwRemain;
        stDisk.strMount = sTmpBuf;
        g_stDiskInfo.insert(std::pair<std::string, TDiskInfo>(strDev, stDisk));
	}
	pclose(fp);
	return 0;
}

int ReadDiskStatInfo(TDiskStatInfo *pInfo, int iMaxDiskInfo)
{
    static bool s_bNotSupport = false;
    if(s_bNotSupport)
        return -1;

	FILE *fp = popen("cat /proc/diskstats", "r");
    if(!fp) {
        MtReport_Log_Error("popen failed, msg:%s", strerror(errno));
        s_bNotSupport = true;
        return -1;
    }

    int i = 0;
    char sLineBuf[1024] = {0};
    for(i=0; i < iMaxDiskInfo && fgets(sLineBuf, sizeof(sLineBuf), fp); i++) {
        if(sscanf(sLineBuf, 
            "%d %d %s %u %u %u %u %u %u %u %u %d %u %u"
             , &pInfo->iDevMajorNum
             , &pInfo->iDevSubNum
             , pInfo->szDevName
             , &pInfo->dwReadTimes
             , &pInfo->dwMergeReadTimes
             , &pInfo->dwReadSectors
             , &pInfo->dwReadTimeMs
             , &pInfo->dwWriteTimes
             , &pInfo->dwMergeWriteTimes
             , &pInfo->dwWriteSectors
             , &pInfo->dwWriteTimeMs
             , &pInfo->iIOWaitCount
             , &pInfo->dwIOTimeMs
             , &pInfo->dwIOTotalTimeMs) != 14) 
        {
            break;
        }

        MtReport_Log_Debug("read diskstat info - %d %d %s %u %u %u %u %u %u %u %u %d %u %u",
            pInfo->iDevMajorNum, pInfo->iDevSubNum, pInfo->szDevName, pInfo->dwReadTimes, pInfo->dwMergeReadTimes,
            pInfo->dwReadSectors, pInfo->dwReadTimeMs, pInfo->dwWriteTimes, pInfo->dwMergeWriteTimes,
            pInfo->dwWriteSectors, pInfo->dwWriteTimeMs, pInfo->iIOWaitCount, pInfo->dwIOTimeMs, pInfo->dwIOTotalTimeMs);
        pInfo ++;
    }
    pclose(fp);
    return i;
}


