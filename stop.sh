#!/bin/bash

proc=xrk_linux_base
count=0
touch _manual_stop_
while [ $count -lt 10 ]
do
	pid=`ps -ef |grep $proc|grep -v tail|grep -v grep|awk '{print $2;}'`
	if [ -z "$pid"  ]; then
		echo "stop ok -- loop try:$count"
		exit
	fi
	for id in $pid
	do
		echo "stop $proc pid:$id ..."
		if [ $count -gt 2 ]; then
			kill -9 $pid > /dev/null 2>&1
		else
			kill -s 10 $pid > /dev/null 2>&1
		fi
	done
	sleep 1
	count=`expr $count + 1`
done
echo "stop failed !"

