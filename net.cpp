/*** xrkmonitor license ***

   Copyright (c) 2019 by rockdeng

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   字符云监控(xrkmonitor) 开源版 (c) 2019 by rockdeng
   当前版本：v1.0
   使用授权协议： apache license 2.0

   云版本主页：http://xrkmonitor.com

   云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
   微信等多种方式，欢迎使用

   内置监控插件 linux_base 功能:
   		使用监控系统 api 实现 linux 基础信息监控上报, 包括 cpu/内存/磁盘/网络

****/

#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/shm.h>
#include <mt_report.h>
#include <iostream>
#include <map>
#include "xrk_linux_base.h"
#include "net.h"

extern uint32_t g_dwCurTimeSec;
extern int g_iTableNetDevAllStaticTime;
extern int g_iTableNetTuPackStatStaticTime;

extern TBaseInfo g_stBaseInfo;

static TNetIfInfo s_eth0 = {0};
static TNetIfInfo s_eth1 = {0};
static TNetIfInfo s_lo = {0};
static TNetIfInfo s_eth_comm0 = {0}; // 网卡名不是 lo,eth0,eth1 的网卡
static TNetIfInfo s_eth_comm1 = {0}; // 网卡名不是 lo,eth0,eth1 的网卡
static TNetIfInfo s_total = {0}; // 全部网卡统计

extern int g_iNotMonitorNetLo;

static std::map<std::string, TNetIfInfo> s_allNetDev;
static TTcpUdpInfo s_stTableTuStatInfo;


void InitGetNet()
{
	// cmd: /bin/cat /proc/net/dev |awk '{if(NR!=1 && NR!=2) print $0 }' |sed 's/:/ /g' |awk '{ print $1" "$2" "$3" "$5" "$10" "$11" "$13}'
	FILE *fp = popen("/bin/cat /proc/net/dev |awk \'{ if(NR!=1 && NR!=2) print $0 }\' "
        "|sed 's/:/ /g'| awk \'{ print $1\" \"$2\" \"$3\" \"$5\" \"$10\" \"$11\" \"$13}\'", "r");
	if(fp == NULL) {
        MtReport_Log_Error("popen failed, msg:%s", strerror(errno));
		return ;
	}
	strncpy(s_total.szInterName, "eth_total", sizeof(s_total.szInterName));

	TNetIfInfo stTmp;
	bool bUseComm0 = false;
	bool bUseComm1 = false;
	while( fscanf(fp, "%s%"PRIu64"%"PRIu64"%u%"PRIu64"%"PRIu64"%u",
		stTmp.szInterName, &stTmp.qwRecvBytes, &stTmp.qwRecvPackets, &stTmp.dwRecvDropPackets, 
		&stTmp.qwSendBytes, &stTmp.qwSendPackets, &stTmp.dwSendDropPackets) == 7)
	{
        s_allNetDev.insert(std::pair<std::string, TNetIfInfo>(stTmp.szInterName, stTmp));

		if(strstr(stTmp.szInterName, "eth0"))
			memcpy(&s_eth0, &stTmp, sizeof(stTmp));
		else if(strstr(stTmp.szInterName, "eth1"))
			memcpy(&s_eth1, &stTmp, sizeof(stTmp));
		else if(strstr(stTmp.szInterName, "lo")) {
            if(g_iNotMonitorNetLo)
                continue;
			memcpy(&s_lo, &stTmp, sizeof(stTmp));
        }
		else {
			MtReport_Log_Info("unknow net interface:%s", stTmp.szInterName);
			if(!bUseComm0) {
				memcpy(&s_eth_comm0, &stTmp, sizeof(stTmp));
				bUseComm0 = true;
			}else if(!bUseComm1) {
				memcpy(&s_eth_comm1, &stTmp, sizeof(stTmp));
				bUseComm1 = true;
			}
		}

		MtReport_Log_Debug("read inter:%s, recv - bytes:%"PRIu64" packets:%"PRIu64" drop:%u ; send - "
			"bytes:%"PRIu64" packets:%"PRIu64" drop:%u", 
			stTmp.szInterName, stTmp.qwRecvBytes,  stTmp.qwRecvPackets, stTmp.dwRecvDropPackets,
			stTmp.qwSendBytes, stTmp.qwSendPackets, stTmp.dwSendDropPackets);

        // lo 不计入总流量、总包量
		if(strstr(stTmp.szInterName, "lo"))
            continue;

		s_total.qwRecvPackets += stTmp.qwRecvPackets;
		s_total.qwSendPackets += stTmp.qwSendPackets;
		s_total.qwRecvBytes += stTmp.qwRecvBytes;
		s_total.qwSendBytes += stTmp.qwSendBytes;
		s_total.dwRecvDropPackets += stTmp.dwRecvDropPackets;
		s_total.dwSendDropPackets += stTmp.dwSendDropPackets;
	}
	pclose(fp);
}

int GetNetTcpUdpInfo(TTcpUdpInfo *pstTcpUdpInfo)
{
    static uint32_t s_dwLastRepTableTuInfoTime = 0;

    static bool b_NotSupport = false;
    if(b_NotSupport)
        return -1;

    // cat /proc/net/snmp  |grep Tcp: |awk '{print $10" "$11" "$12" "$13" "$14" "$15}'
    static const char *ptcp_fields = "CurrEstab InSegs OutSegs RetransSegs InErrs OutRsts";
    static int iTcpFieldsLen = (int)strlen(ptcp_fields);
    static const char *ptcp_cmd = 
        "cat /proc/net/snmp  |grep Tcp: |awk \'{print $10\" \"$11\" \"$12\" \"$13\" \"$14\" \"$15}\'";
    TTcpUdpInfo & stNetTcpUdpInfo = *pstTcpUdpInfo;
    char sFields[256] = {0};
	FILE *fp = popen(ptcp_cmd, "r");
    if(!fp) {
        MtReport_Log_Error("popen(%s) failed, msg:%s", ptcp_cmd, strerror(errno));
        b_NotSupport = true;
        return -1;
    }
    else {
        fgets(sFields, sizeof(sFields), fp);
        if(sFields[0] == '\0' || strncasecmp(sFields, ptcp_fields, iTcpFieldsLen)) {
            MtReport_Log_Warn("invalid tcp info, fields(%s) not match", sFields);
            pclose(fp);
            return -2;
        }

        fscanf(fp, "%d %u %u %u %u %u", 
            &stNetTcpUdpInfo.iCurTcpEstab, &stNetTcpUdpInfo.dwTcpPackIn, &stNetTcpUdpInfo.dwTcpPackOut,
            &stNetTcpUdpInfo.dwTcpReSends, &stNetTcpUdpInfo.dwTcpInErr, &stNetTcpUdpInfo.dwTcpOutRsts);

        MtReport_Log_Debug("get tcp info:%d %u %u %u %u %u", 
            stNetTcpUdpInfo.iCurTcpEstab, stNetTcpUdpInfo.dwTcpPackIn, stNetTcpUdpInfo.dwTcpPackOut,
            stNetTcpUdpInfo.dwTcpReSends, stNetTcpUdpInfo.dwTcpInErr, stNetTcpUdpInfo.dwTcpOutRsts);
        pclose(fp);
    }

    // cat /proc/net/snmp  |grep Udp:|awk '{print $2" "$3" "$4" "$5" "$6" "$7}' 
    static const char *pudp_fields = "InDatagrams NoPorts InErrors OutDatagrams RcvbufErrors SndbufErrors";
    static int iUdpFieldsLen = (int)strlen(pudp_fields);
    static const char *pudp_cmd = 
        "cat /proc/net/snmp  |grep Udp:|awk \'{print $2\" \"$3\" \"$4\" \"$5\" \"$6\" \"$7}\'";
	fp = popen(pudp_cmd, "r");
    if(!fp) {
        MtReport_Log_Error("popen(%s) failed, msg:%s", pudp_cmd, strerror(errno));
        return -3;
    }
    else {
        sFields[0] = '\0';
        fgets(sFields, sizeof(sFields), fp);
        if(sFields[0] == '\0' || strncasecmp(sFields, pudp_fields, iUdpFieldsLen)) {
            MtReport_Log_Warn("invalid udp info, fields(%s) not match", sFields);
            pclose(fp);
            return -4;
        }
        fscanf(fp, "%u %u %u %u %u %u", 
            &stNetTcpUdpInfo.dwUdpPackIn, &stNetTcpUdpInfo.dwUdpNoPorts, &stNetTcpUdpInfo.dwUdpInErr,
            &stNetTcpUdpInfo.dwUdpPackOut, &stNetTcpUdpInfo.dwUdpRecvBufErr, &stNetTcpUdpInfo.dwUdpSendBufErr);
        MtReport_Log_Debug("get udp info: %u %u %u %u %u %u", 
            stNetTcpUdpInfo.dwUdpPackIn, stNetTcpUdpInfo.dwUdpNoPorts, stNetTcpUdpInfo.dwUdpInErr,
            stNetTcpUdpInfo.dwUdpPackOut, stNetTcpUdpInfo.dwUdpRecvBufErr, stNetTcpUdpInfo.dwUdpSendBufErr);
        pclose(fp);
    }

    if(!s_dwLastRepTableTuInfoTime) {
        s_dwLastRepTableTuInfoTime = g_dwCurTimeSec;
        memcpy(&s_stTableTuStatInfo, pstTcpUdpInfo, sizeof(TTcpUdpInfo));
    }
    else if(s_dwLastRepTableTuInfoTime+g_iTableNetTuPackStatStaticTime < g_dwCurTimeSec)
    {
        int iRet = MtReport_Plugin_Table(
            XRK_PLUGIN_ID, XRK_TABLE_NET_TU_PACK_STAT, g_iTableNetTuPackStatStaticTime, g_dwCurTimeSec,
            "%d#%u$%d#%u$%d#%u$%d#%d$%d#%u$%d#%u$%d#%u",
            XRK_FLD_TCP_IN_COUNT, stNetTcpUdpInfo.dwTcpPackIn-s_stTableTuStatInfo.dwTcpPackIn,
            XRK_FLD_TCP_OUT_COUNT, stNetTcpUdpInfo.dwTcpPackOut-s_stTableTuStatInfo.dwTcpPackOut,
            XRK_FLD_TCP_ERR_COUNT, stNetTcpUdpInfo.dwTcpReSends-s_stTableTuStatInfo.dwTcpReSends+
                stNetTcpUdpInfo.dwTcpInErr-s_stTableTuStatInfo.dwTcpInErr+
                stNetTcpUdpInfo.dwTcpOutRsts-s_stTableTuStatInfo.dwTcpOutRsts,
            XRK_FLD_TCP_CONN_COUNT, stNetTcpUdpInfo.iCurTcpEstab,
            XRK_FLD_UDP_IN_COUNT, stNetTcpUdpInfo.dwUdpPackIn-s_stTableTuStatInfo.dwUdpPackIn,
            XRK_FLD_UDP_OUT_COUNT, stNetTcpUdpInfo.dwUdpPackOut-s_stTableTuStatInfo.dwUdpPackOut,
            XRK_FLD_UDP_ERR_COUNT, stNetTcpUdpInfo.dwUdpInErr-s_stTableTuStatInfo.dwUdpInErr+
                stNetTcpUdpInfo.dwUdpNoPorts-s_stTableTuStatInfo.dwUdpNoPorts+
                stNetTcpUdpInfo.dwUdpRecvBufErr-s_stTableTuStatInfo.dwUdpRecvBufErr+
                stNetTcpUdpInfo.dwUdpSendBufErr-s_stTableTuStatInfo.dwUdpSendBufErr);
        if(iRet != 0)
            MtReport_Log_Error("report realtime table(%d) failed, ret:%d", XRK_TABLE_NET_DEV_ALL, iRet);
        s_dwLastRepTableTuInfoTime = g_dwCurTimeSec;
        memcpy(&s_stTableTuStatInfo, &stNetTcpUdpInfo, sizeof(TTcpUdpInfo));
    }

    return 0;
}

void ReportNetInfo()
{
    static uint32_t s_dwLastTableNetRepTime = g_dwCurTimeSec;
    static bool b_NotSupport = false;
    if(b_NotSupport)
        return ;

	// cmd: /bin/cat /proc/net/dev |awk '{if(NR!=1 && NR!=2) print $0 }' |sed 's/:/ /g' |awk '{ print $1" "$2" "$3" "$5" "$10" "$11" "$13}'
	FILE *fp = popen("/bin/cat /proc/net/dev |awk \'{ if(NR!=1 && NR!=2) print $0 }\' |sed 's/:/ /g'| awk \'{ print $1\" \"$2\" \"$3\" \"$5\" \"$10\" \"$11\" \"$13}\'", "r");
	if(fp == NULL) {
        MtReport_Log_Error("popen failed, msg:%s", strerror(errno));
        b_NotSupport = true;
		return ;
	}

	TNetIfInfo stTmp_total;
	memset(&stTmp_total, 0, sizeof(stTmp_total));
	strncpy(stTmp_total.szInterName, "eth_total", sizeof(stTmp_total.szInterName));

	TNetIfInfo stTmp;
	bool bUseComm0 = false;
	bool bUseComm1 = false;
    int iRet = 0;

    // 网络接口情况实时表格统计
    bool bRepTableNetInfo = false;
    if(g_dwCurTimeSec >= s_dwLastTableNetRepTime+g_iTableNetDevAllStaticTime) {
        bRepTableNetInfo = true;
        s_dwLastTableNetRepTime = g_dwCurTimeSec;
    }

	while( fscanf(fp, "%s%"PRIu64"%"PRIu64"%u%"PRIu64"%"PRIu64"%u",
		stTmp.szInterName, &stTmp.qwRecvBytes, &stTmp.qwRecvPackets, &stTmp.dwRecvDropPackets, 
		&stTmp.qwSendBytes, &stTmp.qwSendPackets, &stTmp.dwSendDropPackets) == 7)
	{
        if(bRepTableNetInfo) {
            std::map<std::string, TNetIfInfo>::iterator it = s_allNetDev.find(stTmp.szInterName);
            if(it != s_allNetDev.end()) {
                TNetIfInfo &last = it->second;
                iRet = MtReport_Plugin_Table(
                    XRK_PLUGIN_ID, XRK_TABLE_NET_DEV_ALL, g_iTableNetDevAllStaticTime, g_dwCurTimeSec,
                    "%d#%s$%d#%lu$%d#%lu$%d#%lu$%d#%lu",
                    XRK_FLD_NET_DEV_NAME, XRK_PLUGIN_TABLE_STR_CHANGE(stTmp.szInterName),
                    XRK_FLD_NET_DEV_RECV_BYTES, stTmp.qwRecvBytes-last.qwRecvBytes,
                    XRK_FLD_NET_DEV_SEND_BYTES, stTmp.qwSendBytes-last.qwSendBytes,
                    XRK_FLD_NET_DEV_RECV_PACKS, stTmp.qwRecvPackets-last.qwRecvPackets,
                    XRK_FLD_NET_DEV_SEND_PACKS, stTmp.qwSendPackets-last.qwSendPackets); 
                if(iRet != 0)
                    MtReport_Log_Error("report realtime table(%d) failed, ret:%d", XRK_TABLE_NET_DEV_ALL, iRet);
                memcpy(&last, &stTmp, sizeof(stTmp));
            }
            else {
                MtReport_Log_Warn("not find dev :%s in allnetdev", stTmp.szInterName);
                s_allNetDev.insert(std::pair<std::string, TNetIfInfo>(stTmp.szInterName, stTmp));
            }
        }

		if(strstr(stTmp.szInterName, "eth0"))
		{
			MtReport_Attr_Add(ETH0_IN_PACK, stTmp.qwRecvPackets-s_eth0.qwRecvPackets);
			MtReport_Attr_Add(ETH0_IN_BYTES, stTmp.qwRecvBytes-s_eth0.qwRecvBytes);
			MtReport_Attr_Add(ETH0_OUT_PACK, stTmp.qwSendPackets-s_eth0.qwSendPackets);
			MtReport_Attr_Add(ETH0_OUT_BYTES, stTmp.qwSendBytes-s_eth0.qwSendBytes);
			memcpy(&s_eth0, &stTmp, sizeof(stTmp));
		}
		else if(strstr(stTmp.szInterName, "eth1"))
		{
			MtReport_Attr_Add(ETH1_IN_PACK, stTmp.qwRecvPackets-s_eth1.qwRecvPackets);
			MtReport_Attr_Add(ETH1_IN_BYTES, stTmp.qwRecvBytes-s_eth1.qwRecvBytes);
			MtReport_Attr_Add(ETH1_OUT_PACK, stTmp.qwSendPackets-s_eth1.qwSendPackets);
			MtReport_Attr_Add(ETH1_OUT_BYTES, stTmp.qwSendBytes-s_eth1.qwSendBytes);
			memcpy(&s_eth1, &stTmp, sizeof(stTmp));
		}
		else if(strstr(stTmp.szInterName, "lo"))
		{
            if(g_iNotMonitorNetLo)
                continue;
            MtReport_Attr_Add(LO_IN_PACK, stTmp.qwRecvPackets-s_lo.qwRecvPackets);
            MtReport_Attr_Add(LO_IN_BYTES, stTmp.qwRecvBytes-s_lo.qwRecvBytes);
            MtReport_Attr_Add(LO_OUT_PACK, stTmp.qwSendPackets-s_lo.qwSendPackets);
			MtReport_Attr_Add(LO_OUT_BYTES, stTmp.qwSendBytes-s_lo.qwSendBytes);
			memcpy(&s_lo, &stTmp, sizeof(stTmp));
		}
		else {
			MtReport_Log_Info("unknow net interface:%s", stTmp.szInterName);
			if(!bUseComm0) {
				MtReport_Attr_Add(ETH_COMM0_IN_PACK, stTmp.qwRecvPackets-s_eth_comm0.qwRecvPackets);
				MtReport_Attr_Add(ETH_COMM0_IN_BYTES, stTmp.qwRecvBytes-s_eth_comm0.qwRecvBytes);
				MtReport_Attr_Add(ETH_COMM0_OUT_PACK, stTmp.qwSendPackets-s_eth_comm0.qwSendPackets);
				MtReport_Attr_Add(ETH_COMM0_OUT_BYTES, stTmp.qwSendBytes-s_eth_comm0.qwSendBytes);
				memcpy(&s_eth_comm0, &stTmp, sizeof(stTmp));
				bUseComm0 = true;
			}else if(!bUseComm1) {
				MtReport_Attr_Add(ETH_COMM1_IN_PACK, stTmp.qwRecvPackets-s_eth_comm1.qwRecvPackets);
				MtReport_Attr_Add(ETH_COMM1_IN_BYTES, stTmp.qwRecvBytes-s_eth_comm1.qwRecvBytes);
				MtReport_Attr_Add(ETH_COMM1_OUT_PACK, stTmp.qwSendPackets-s_eth_comm1.qwSendPackets);
				MtReport_Attr_Add(ETH_COMM1_OUT_BYTES, stTmp.qwSendBytes-s_eth_comm1.qwSendBytes);
				memcpy(&s_eth_comm1, &stTmp, sizeof(stTmp));
				bUseComm1 = true;
			}
		}

		MtReport_Log_Debug("read inter:%s, recv - bytes:%"PRIu64" packets:%"PRIu64" drop:%u ; send - "
			"bytes:%"PRIu64" packets:%"PRIu64" drop:%u", 
			stTmp.szInterName, stTmp.qwRecvBytes,  stTmp.qwRecvPackets, stTmp.dwRecvDropPackets,
			stTmp.qwSendBytes, stTmp.qwSendPackets, stTmp.dwSendDropPackets);
	
        // lo 不计入总流量、总包量
		if(strstr(stTmp.szInterName, "lo")) 
            continue;

		stTmp_total.qwRecvPackets += stTmp.qwRecvPackets;
		stTmp_total.qwSendPackets += stTmp.qwSendPackets;
		stTmp_total.qwRecvBytes += stTmp.qwRecvBytes;
		stTmp_total.qwSendBytes += stTmp.qwSendBytes;
		stTmp_total.dwRecvDropPackets += stTmp.dwRecvDropPackets;
		stTmp_total.dwSendDropPackets += stTmp.dwSendDropPackets;
	}
	pclose(fp);

    // 表格基础资源概况
    g_stBaseInfo.qwSendBytes += stTmp_total.qwSendBytes-s_total.qwSendBytes;
    g_stBaseInfo.qwRecvBytes += stTmp_total.qwRecvBytes-s_total.qwRecvBytes;

	MtReport_Attr_Add(ETHTOTAL_IN_PACK, stTmp_total.qwRecvPackets-s_total.qwRecvPackets);
	MtReport_Attr_Add(ETHTOTAL_IN_BYTES, stTmp_total.qwRecvBytes-s_total.qwRecvBytes);
	MtReport_Attr_Add(ETHTOTAL_OUT_PACK, stTmp_total.qwSendPackets-s_total.qwSendPackets);
	MtReport_Attr_Add(ETHTOTAL_OUT_BYTES, stTmp_total.qwSendBytes-s_total.qwSendBytes);
	if(stTmp_total.dwSendDropPackets > s_total.dwSendDropPackets
		|| stTmp_total.dwRecvDropPackets > s_total.dwRecvDropPackets)
	{
		MtReport_Log_Info("get drop packet send:%u, recv:%u", stTmp_total.dwSendDropPackets-s_total.dwSendDropPackets,
			stTmp_total.dwRecvDropPackets-s_total.dwRecvDropPackets);
		MtReport_Attr_Add(NETIF_DROP_PACK, 1);
	}
	memcpy(&s_total, &stTmp_total, sizeof(stTmp_total));
}


