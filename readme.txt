xrkmonitor 监控插件： linux_base

功能：
该插件提供 linux 操作系统的基础资源监控， 包括cpu/内存/磁盘/网络 等

编译说明：
该插件同时适用于开源版和云版本， 可通过 Makefile 文件中的 env_make_type 指定编译版本
默认编译开源版监控插件 (env_make_type 设为 cloud 表示编译云版本插件)

编译开源版插件：请先下下载开源版源码，将该插件放在开源版源码目录中
编译云版本插件：请先在云版本下载中心下载 c/c++ 开发库解压后执行 install.sh 脚本

