/*** xrkmonitor license ***

   Copyright (c) 2019 by rockdeng

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   字符云监控(xrkmonitor) 开源版 (c) 2019 by rockdeng
   当前版本：v1.0
   使用授权协议： apache license 2.0

   云版本主页：http://xrkmonitor.com

   云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
   微信等多种方式，欢迎使用

   内置监控插件 linux_base 功能:
   		使用监控系统 api 实现 linux 基础信息监控上报, 包括 cpu/内存/磁盘/网络

****/

#define __STDC_FORMAT_MACROS
#include <sstream>
#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/shm.h>
#include <mt_report.h>
#include "cpu.h"

extern uint32_t g_dwCurTimeSec;
extern int g_iTableCpuAllStatStaticTime;
extern char g_szCpuInfo[32];

static TCpuStatic s_cpuLastInfoForTable;

const char * GetCmdResult(const char *cmd);

void GetCpuInfo()
{
    static const char *cpu_cmd = "cat /proc/cpuinfo |awk -F \":\" \'{ if(match($1, \"cpu MHz\")) print \"1 \"$2/1000; else if(match($1, \"cpu cores\")) print \"2 \"$2; }\'";

	FILE *fp = popen(cpu_cmd, "r");
	if(fp == NULL) {
        MtReport_Log_Error("popen failed, msg:%s", strerror(errno));
		return ;
	}

    int iCpuNum = 0, iNum = 0;
    float ftmp = 0.0, fcpuGhz = 0.0;
	while(fscanf(fp, "%d %f", &iNum, &ftmp) == 2)
	{
        if(iNum == 1)
            fcpuGhz = ftmp;
        else if(iNum == 2)
            iCpuNum = (int)ftmp;
	}
	pclose(fp);
    snprintf(g_szCpuInfo, sizeof(g_szCpuInfo), "%d核/%.2fGHz", iCpuNum, fcpuGhz);
}


int GetCpuStatis(TCpuStatic *pcp)
{
	TCpuInfo *plast = (TCpuInfo*)pcp->sInfo;
    const char *pct = GetCmdResult("cat /proc/stat");
    if(!pct)
        return -2;

    std::ostringstream ss;
    ss << "echo \"" << pct << "\" |grep cpu|awk \'{print "CPU_AWK_FMT"}\'";
	FILE *fp = popen(ss.str().c_str(), "r");
	if(fp == NULL) {
        MtReport_Log_Error("popen failed, msg:%s", strerror(errno));
		return -2;
	}

	int i=0;
	while( fscanf(fp, 
        "%s %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64" %"PRIu64,
		plast[i].szCpuName, &plast[i].qwUser, &plast[i].qwNice, &plast[i].qwSys, &plast[i].qwIdle,
		&plast[i].qwIowait, &plast[i].qwIrq, &plast[i].qwSoftIrq, &plast[i].qwSteal,
        &plast[i].qwGuest, &plast[i].qwGuestNice) == 11) 
	{
		plast[i].qwTotal = plast[i].qwUser + plast[i].qwNice + plast[i].qwSys
			+ plast[i].qwIdle + plast[i].qwIowait + plast[i].qwIrq + plast[i].qwSoftIrq
            + plast[i].qwSteal + plast[i].qwGuest + plast[i].qwGuestNice;

        MtReport_Log_Debug("get cpu(%d, %s) static info - user:%"PRIu64" nice:%"PRIu64" sys:%"PRIu64" idle:%"PRIu64
            " iowait:%"PRIu64" icq:%"PRIu64" softirq:%"PRIu64" steal:%"PRIu64" guest:%"PRIu64" guest nice:%"PRIu64, i, 
            plast[i].szCpuName, plast[i].qwUser, plast[i].qwNice, plast[i].qwSys, plast[i].qwIdle, plast[i].qwIowait, 
            plast[i].qwIrq, plast[i].qwSoftIrq, plast[i].qwSteal, plast[i].qwGuest, plast[i].qwGuestNice);

		i++;
		if(i > MAX_CPU_SUPPORT) {
			MtReport_Log_Warn("cpu monitor over limit");
			break;
		}
	}
	pclose(fp);
	pcp->iCpuCount = i;

    ss.str("");
    ss << "echo \"" << pct << "\" |grep ctxt|awk \'{print $2}\'";
    fp = popen(ss.str().c_str(), "r");
    if(fp == NULL) {
        MtReport_Log_Error("popen failed, msg:%s", strerror(errno));
		return -3;
	}
    fscanf(fp, "%"PRIu64, &pcp->qwCtxt);
    pclose(fp);

    ss.str("");
    ss << "echo \"" << pct << "\" |grep processes|awk \'{print $2}\'";
    fp = popen(ss.str().c_str(), "r");
    if(fp == NULL) {
        MtReport_Log_Error("popen failed, msg:%s", strerror(errno));
		return -4;
	}
    fscanf(fp, "%"PRIu32, &pcp->dwProcessCount);
    pclose(fp);
    MtReport_Log_Debug("get context switch:%"PRIu64" processes:%"PRIu32, pcp->qwCtxt, pcp->dwProcessCount);
	return i;
}

static TCpuStatic s_cpuLastInfo;
int InitGetCpuUse()
{
	if(s_cpuLastInfo.iCpuCount > 0)
		return 0;
	if(GetCpuStatis(&s_cpuLastInfo) < 0)
		return -1;
	if(s_cpuLastInfo.iCpuCount <= 0)
		return -2;
    memcpy(&s_cpuLastInfoForTable, &s_cpuLastInfo, sizeof(s_cpuLastInfo));
	return s_cpuLastInfo.iCpuCount;
}

int GetCpuUse(TcpuUse *pCpuUse)
{
    static uint32_t s_dwLastTableCpuStat = g_dwCurTimeSec;

	TCpuStatic now;
	if(GetCpuStatis(&now) < 0)
		return -1;
	if(now.iCpuCount != s_cpuLastInfo.iCpuCount)
		return -2;

    // cpu 状态实时表格 -- start
    if(g_dwCurTimeSec >= s_dwLastTableCpuStat+g_iTableCpuAllStatStaticTime) {
        int iCpuUser, iCpuNice, iCpuSys, iCpuIoWa, iCpuIrq, iCpuSoftIrq, iCpuGuest, iCpuGuestNice, iCpuUse;
        for(int i=0, iRet=0; i < now.iCpuCount; i++) 
        {
#define GET_CPU_AVG_USE(f) do { \
    iRet = f / 10; \
    if(f%10) \
        iRet++; \
    f = iRet; \
}while(0)
            if(now.sInfo[i].qwTotal > s_cpuLastInfoForTable.sInfo[i].qwTotal) {
                iCpuUser = CPU_USER(s_cpuLastInfoForTable.sInfo[i], now.sInfo[i]);
                GET_CPU_AVG_USE(iCpuUser);

                iCpuNice = CPU_NICE(s_cpuLastInfoForTable.sInfo[i], now.sInfo[i]);
                GET_CPU_AVG_USE(iCpuNice);

                iCpuSys = CPU_SYS(s_cpuLastInfoForTable.sInfo[i], now.sInfo[i]);
                GET_CPU_AVG_USE(iCpuSys);

                iCpuIoWa = CPU_IO(s_cpuLastInfoForTable.sInfo[i], now.sInfo[i]);
                GET_CPU_AVG_USE(iCpuIoWa);

                iCpuIrq = CPU_IRQ(s_cpuLastInfoForTable.sInfo[i], now.sInfo[i]);
                GET_CPU_AVG_USE(iCpuIrq);

                iCpuSoftIrq = CPU_SOFTIRQ(s_cpuLastInfoForTable.sInfo[i], now.sInfo[i]);
                GET_CPU_AVG_USE(iCpuSoftIrq);

                iCpuGuest = CPU_GUEST(s_cpuLastInfoForTable.sInfo[i], now.sInfo[i]);
                GET_CPU_AVG_USE(iCpuGuest);

                iCpuGuestNice = CPU_GUESTNI(s_cpuLastInfoForTable.sInfo[i], now.sInfo[i]);
                GET_CPU_AVG_USE(iCpuGuestNice);

                iCpuUse = CPU_USE(s_cpuLastInfoForTable.sInfo[i], now.sInfo[i]);
                GET_CPU_AVG_USE(iCpuUse);
            }
            else {
                iCpuUser = 0;
                iCpuNice = 0;
                iCpuSys = 0;
                iCpuIoWa = 0;
                iCpuIrq = 0;
                iCpuSoftIrq = 0;
                iCpuGuest = 0;
                iCpuGuestNice = 0;
                iCpuUse = 0;
            }
#undef GET_CPU_AVG_USE

            iRet = MtReport_Plugin_Table(
                XRK_PLUGIN_ID, XRK_TABLE_CPU_ALL_STAT, g_iTableCpuAllStatStaticTime, g_dwCurTimeSec,
                "%d#%s$%d#%d$%d#%d$%d#%d$%d#%d$%d#%d$%d#%d$%d#%d$%d#%d$%d#%d",
                XRK_FLD_CPU_NUM, XRK_PLUGIN_TABLE_STR_CHANGE(now.sInfo[i].szCpuName),
                XRK_FLD_CPU_USER, iCpuUser, XRK_FLD_CPU_NICE, iCpuNice, XRK_FLD_CPU_SYS, iCpuSys,
                XRK_FLD_CPU_IOWA, iCpuIoWa, XRK_FLD_CPU_IRQ, iCpuIrq, XRK_FLD_CPU_SOFTIRQ, iCpuSoftIrq,
                XRK_FLD_CPU_GUEST, iCpuGuest, XRK_FLD_CPU_GUESTNICE, iCpuGuestNice, XRK_FLD_CPU_STAT_USE, iCpuUse);
            if(iRet != 0) {
                MtReport_Log_Error("report realtime table(%d) failed, ret:%d", XRK_TABLE_CPU_ALL_STAT, iRet);
                break;
            }

            /*
            if(i == 0) {
                MtReport_Log_Debug("cpu stat table - user:%d, nice:%d, sys:%d, iowa:%d, irq:%d, softirq:%d, "
                    "guest:%d, guestni:%d, cpu use:%d",
                    iCpuUser, iCpuNice, iCpuSys, iCpuIoWa, iCpuIrq, iCpuSoftIrq, pCpuUse->iCpuGuest,
                    iCpuGuestNice, iCpuUse);
            }
            */
        }

        memcpy(&s_cpuLastInfoForTable, &now, sizeof(now));
        s_dwLastTableCpuStat = g_dwCurTimeSec;
    }
    // cpu 状态实时表格 -- end 

	TCpuStatic *ppre = &s_cpuLastInfo;
	TCpuStatic *pnowcp = &now;
	pCpuUse->iCpuCount = pnowcp->iCpuCount;

    // CPU 整合各场景的使用率
    if(pnowcp->sInfo[0].qwTotal > ppre->sInfo[0].qwTotal) {
        pCpuUse->iCpuUser = CPU_USER(ppre->sInfo[0], pnowcp->sInfo[0]);
        pCpuUse->iCpuNice = CPU_NICE(ppre->sInfo[0], pnowcp->sInfo[0]);
        pCpuUse->iCpuSys = CPU_SYS(ppre->sInfo[0], pnowcp->sInfo[0]);
        //pCpuUse->iCpuIdle = CPU_IDLE(ppre->sInfo[0], pnowcp->sInfo[0]);
        pCpuUse->iCpuIoWa = CPU_IO(ppre->sInfo[0], pnowcp->sInfo[0]);
        pCpuUse->iCpuIrq = CPU_IRQ(ppre->sInfo[0], pnowcp->sInfo[0]);
        pCpuUse->iCpuSoftIrq = CPU_SOFTIRQ(ppre->sInfo[0], pnowcp->sInfo[0]);
        pCpuUse->iCpuSteal = CPU_STEAL(ppre->sInfo[0], pnowcp->sInfo[0]);
        pCpuUse->iCpuGuest = CPU_GUEST(ppre->sInfo[0], pnowcp->sInfo[0]);
        pCpuUse->iCpuGuestNice = CPU_GUESTNI(ppre->sInfo[0], pnowcp->sInfo[0]);
    }

    if(pnowcp->qwCtxt > ppre->qwCtxt)
        pCpuUse->dwCtxt = pnowcp->qwCtxt - ppre->qwCtxt;
    if(pnowcp->dwProcessCount > ppre->dwProcessCount)
        pCpuUse->dwProcessNew = pnowcp->dwProcessCount - ppre->dwProcessCount;

	for(int i=0; i < pnowcp->iCpuCount; i++) {
		if(ppre->sInfo[i].qwTotal == pnowcp->sInfo[i].qwTotal) {
			pCpuUse->iCpuUse[i] = 0;
            continue;
		}
		pCpuUse->iCpuUse[i] = CPU_USE(ppre->sInfo[i], pnowcp->sInfo[i]);
	}
	memcpy(ppre, pnowcp, sizeof(TCpuStatic));

    MtReport_Log_Debug("get cpu use- cpu total - user:%d, nice:%d, sys:%d, idle:%d, iowa:%d, irq:%d, softirq:%d, "
        "steal:%d, guest:%d, guestni:%d, new process:%u, ctxt:%u, cpu use:%d",
        pCpuUse->iCpuUser, pCpuUse->iCpuNice, pCpuUse->iCpuSys, pCpuUse->iCpuIdle, 
        pCpuUse->iCpuIoWa, pCpuUse->iCpuIrq, pCpuUse->iCpuSoftIrq, pCpuUse->iCpuSteal, pCpuUse->iCpuGuest,
        pCpuUse->iCpuGuestNice, pCpuUse->dwProcessNew, pCpuUse->dwCtxt, pCpuUse->iCpuUse[0]);

	return pCpuUse->iCpuCount;
}

void WriteCpuLoadAvg()
{
    std::string str("cat /proc/loadavg | awk \'{print $1}\'");
    FILE *fp = popen(str.c_str(), "r");
    if(!fp) {
        MtReport_Log_Error("popen failed, msg:%s", strerror(errno));
        return;
    }
    float f = 0.0;
    if(fscanf(fp, "%f", &f) == 1) {
        MtReport_Attr_Set(XRK_CPU_LOAD_AVG, (int)(f*100));
        MtReport_Log_Info("get cpu load avg:%.2f", f);
    }
    pclose(fp);
}


