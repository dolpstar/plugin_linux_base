# linux_base 

#### 介绍
xrkmonitor 字符云开源监控系统的扩展监控插件  
该插件提供 linux 操作系统的基础资源监控，包括cpu/内存/磁盘/网络等  
控制台支持一键部署，且可在控制台页面执行配置修改、启用禁用、移除等操作。

插件编译方法见 Makefile 文件说明
xrkmonitor 开源项目地址：https://gitee.com/xrkmonitorcom/open


