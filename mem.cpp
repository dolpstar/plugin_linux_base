/*** xrkmonitor license ***

   Copyright (c) 2019 by rockdeng

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.


   字符云监控(xrkmonitor) 开源版 (c) 2019 by rockdeng
   当前版本：v1.0
   使用授权协议： apache license 2.0

   云版本主页：http://xrkmonitor.com

   云版本为开源版提供永久免费告警通道支持，告警通道支持短信、邮件、
   微信等多种方式，欢迎使用

   内置监控插件 linux_base 功能:
   		使用监控系统 api 实现 linux 基础信息监控上报, 包括 cpu/内存/磁盘/网络

****/

#include <stdlib.h>
#include <inttypes.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/shm.h>
#include <mt_report.h>
#include "mem.h"
#include "xrk_linux_base.h"

extern uint32_t g_dwCurTimeSec;
extern int g_iTableMemStatStaticTime;

inline uint32_t ChangeMemToKb(uint32_t v, const char *szUnit)
{
    if(!strcasecmp(szUnit, "B"))
        return v/1024;
    if(!strcasecmp(szUnit, "KB"))
        return v;
    if(!strcasecmp(szUnit, "MB"))
        return v*1024;
    if(!strcasecmp(szUnit, "GB"))
        return v*1024*1024;
    return 0;
}

int GetMemInfo(TMemInfo &mem)
{
    static uint32_t s_dwLastTableMemStatTime = g_dwCurTimeSec;
    static bool bNotSupport = false;
    if(bNotSupport)
        return -1;

	FILE *fp = popen("/bin/cat /proc/meminfo | awk \'{if(NF==3) print $1\" \"$2\" \"$3 }\'", "r");
	if(fp == NULL) {
        MtReport_Log_Error("popen failed, msg:%s !", strerror(errno));
        bNotSupport = true;
		return -1;
	}

	char sMemField[64] = {0}, sMemUnit[16] = {0};
	uint32_t dwValue = 0;
	while( fscanf(fp, "%s%u%s", sMemField, &dwValue, sMemUnit) == 3) {
		if(0 == strcasecmp(sMemField, "MemTotal:"))
		{
			strncpy(mem.szUnit, sMemUnit, sizeof(mem.szUnit));
			mem.dwMemTotal = ChangeMemToKb(dwValue, mem.szUnit);
		}
		else if(0 == strcasecmp(sMemField, "MemFree:")) 
			mem.dwMemFree = ChangeMemToKb(dwValue, mem.szUnit);
		else if(0 == strcasecmp(sMemField, "Buffers:"))
			mem.dwBuffers = ChangeMemToKb(dwValue, mem.szUnit);
		else if(0 == strcasecmp(sMemField, "Cached:"))
			mem.dwCached = ChangeMemToKb(dwValue, mem.szUnit);
		else if(0 == strcasecmp(sMemField, "MemAvailable:"))
			mem.dwMemAvailable = ChangeMemToKb(dwValue, mem.szUnit);
		else if(0 == strcasecmp(sMemField, "SwapTotal:"))
			mem.dwSwapTotal = ChangeMemToKb(dwValue, mem.szUnit);
		else if(0 == strcasecmp(sMemField, "SwapFree:"))
			mem.dwSwapFree = ChangeMemToKb(dwValue, mem.szUnit);
		else if(0 == strcasecmp(sMemField, "Dirty:"))
			mem.dwDirty = ChangeMemToKb(dwValue, mem.szUnit);
		else if(0 == strcasecmp(sMemField, "Mapped:"))
			mem.dwMapped = ChangeMemToKb(dwValue, mem.szUnit);
		else if(0 == strcasecmp(sMemField, "Shmem:"))
			mem.dwShmem = ChangeMemToKb(dwValue, mem.szUnit);
	}
	pclose(fp);

    if(g_dwCurTimeSec >= s_dwLastTableMemStatTime+g_iTableMemStatStaticTime) {
        s_dwLastTableMemStatTime = g_dwCurTimeSec;
        uint32_t dwFree = mem.dwMemFree + mem.dwCached - mem.dwDirty - mem.dwMapped;
        int32_t iUsePer = (int)((mem.dwMemTotal-dwFree)*100/mem.dwMemTotal);
        int iRet = MtReport_Plugin_Table(
            XRK_PLUGIN_ID, XRK_TABLE_MEM_STAT, g_iTableMemStatStaticTime, g_dwCurTimeSec,
            "%d#%u$%d#%u$%d#%u$%d#%u$%d#%u$%d#%u$%d#%u$%d#%u$%d#%d",
            XRK_FLD_MEM_TOTAL, mem.dwMemTotal, XRK_FLD_MEM_FREE, mem.dwMemFree,
            XRK_FLD_MEM_BUFF, mem.dwBuffers, XRK_FLD_MEM_CACHE, mem.dwCached, 
            XRK_FLD_MEM_AVAILABLE, mem.dwMemAvailable, XRK_FLD_MEM_SWAP_TOTAL, mem.dwSwapTotal,
            XRK_FLD_MEM_SWAP_FREE, mem.dwSwapFree, XRK_FLD_MEM_SHM, mem.dwShmem, 
            XRK_FLD_MEM_T_USE_PERCENT, iUsePer);
        if(iRet != 0) 
            MtReport_Log_Error("report realtime table(%d) failed, ret:%d", XRK_TABLE_MEM_STAT, iRet);
    }

	return 0;
}


